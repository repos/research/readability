import json
import pandas as pd
import ast
import pickle 
import spacy

def apply_dbpedia(datapoint_wiki, lang = 'en'):
    nlp = spacy.blank(lang)
    nlp.add_pipe('dbpedia_spotlight', config={'confidence': 0, 'overwrite_ents':False})
    docs = datapoint_wiki['text']
    entities = []
    for text in docs:
        try:
            doc = nlp(text)
            entities.append([(ent.text, ent.kb_id_,
             ent._.dbpedia_raw_result['@similarityScore']) for ent in doc.ents])
        except Exception as e:
            print(e)
            print('!!!!!!!!!!!!!!!!!!!!!!!')
            # print(text)
            pass
        
    return entities

def apply_dbpedia_api(datapoint_wiki, lang = "en", confidence = 0.):
    '''
    Annotating text with dbpedia-spotlight API https://www.dbpedia-spotlight.org/
    '''
    docs = datapoint_wiki['text']
    # available languages: https://stats.uptimerobot.com/zv1QpHxy6
    list_lang_dbpedia = ["ca", "da", "de", "en", "es", "fi", "fr", "hu", "it", "nl", "no", "pt", "ro", "ru", "sv", "tr"]
    if lang not in list_lang_dbpedia:
        print("Entity-linker not supported in language: %s. Pick one of %s"%(lang, list_lang_dbpedia))
        return [[] for h in docs]
    else:
        api_url = "https://api.dbpedia-spotlight.org/%s/annotate"%lang
        headers = {
            "User-Agent": "readability: MGerlach_(WMF)|mgerlach@wikimedia.org",
            "accept": "application/json"
        }

        entities = []
        for text in docs:
            params = {
                "text": text,
                "confidence": confidence,
            }
            try:
                req = requests.get(api_url, headers=headers, params=params).json()
            except Exception as e:
                print(e)
                print('!!!!!!!!!!!!!!!!!!!!!!!')
                req = {}
                # print(text)
    #             pass

            ents_raw = req.get("Resources",[])
            ents_filtered = []
            for ent in ents_raw:
                ent_label = ent.get("@URI")
                ent_mention = ent.get("@surfaceForm")
                ent_score = ent.get("@similarityScore")

                if ent_label != None:
                    ents_filtered += [(ent_label,ent_mention,ent_score)]
            entities += [ents_filtered]
        return entities

def get_entities_and_mentions(datapoint_wiki):
    list_of_entity_types_sent = [] 
    list_of_entity_types_doc = []
    list_of_mention_types_sent = []
    list_of_mention_types_doc = []
    list_of_tokens_sent = []
    list_of_tokens_doc = []
    
    for sent in datapoint_wiki['entities']:
        # tokens = all entities & mentions in a sentence
        entities = [i[1] for i in sent]
        mentions = [i[0] for i in sent] # this is the same as above
        
        # types = all unique entities & mentions in a sentence
        unique_entities = set([i[1] for i in sent])
        unique_mentions = set([i[0] for i in sent]) # this is NOT the same as above
        
        list_of_entity_types_sent.append(unique_entities)
        list_of_entity_types_doc.extend(unique_entities)
        list_of_mention_types_sent.append(unique_mentions)
        list_of_mention_types_doc.extend(unique_mentions)
        
        list_of_tokens_sent.append(entities)
        list_of_tokens_doc.extend(entities)
        
    list_of_entity_types_doc = list(set(list_of_entity_types_doc)) # only have different number of entities / mentions across the doc
    list_of_mention_types_doc = list(set(list_of_mention_types_doc))
        
    return list_of_entity_types_sent, list_of_entity_types_doc, list_of_mention_types_sent, list_of_mention_types_doc,\
           list_of_tokens_sent, list_of_tokens_doc
           
#     return list_of_entities_sent, list_of_entities_doc, list_of_mentions_sent, list_of_mentions_doc

def preprocess(df, scaler = None, test_flag = True):
    from sklearn.preprocessing import StandardScaler
    if test_flag:
        return scaler.transform(df)
    else:
        scaler = StandardScaler()
        transformed_df = scaler.fit_transform(df)
        return transformed_df, scaler


if __name__ == '__main__':
    print()