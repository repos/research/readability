import json
import numpy as np
import pandas as pd
import warnings

warnings.filterwarnings("ignore")

RANDOM_SEED = 42

def build_df_from_list(data_list, shuffle=False):
    """
    Build sentence based dataframe from list of articles
    """
    en_data = [i['hard'] for i in data_list]
    simple_data = [i['easy'] for i in data_list]
    texts = []
    titles = []
    labels = []

    for article in en_data:
        sentences_to_add = article["text"]
        texts += sentences_to_add
        titles += [article["title"]] * len(sentences_to_add)
        labels += [1] * len(sentences_to_add)

    for article in simple_data:
        sentences_to_add = article["text"]
        texts += sentences_to_add
        titles += [article["title"]] * len(sentences_to_add)
        labels += [0] * len(sentences_to_add)

    df = pd.DataFrame({
        "title": titles,
        "text": texts,
        "label": labels
    })
    if shuffle:
        df = df.sample(len(df)) #shuffle
    return df


def balance_df(df, column="label"):
    positive = df[df[column] == 1]
    negative = df[df[column] == 0]
    n_sample = np.min([len(positive), len(negative)])
    positive = positive.sample(n_sample, random_state=RANDOM_SEED)
    negative = negative.sample(n_sample, random_state=RANDOM_SEED)
    return pd.concat([positive, negative]).reset_index(drop=True)


data_lists = []
parts_names = ["train", "test", "validation"]
data_file_pattern = '../../data/simple-en-{}.bz2'

for sample_name in parts_names:
    data_file = data_file_pattern.format(sample_name)
    data = []
    with open(data_file, 'r') as fout:
        for line in fout:
            dict_in = json.loads(line)
            data.append(dict_in)
    data_lists.append(data)


train_df = build_df_from_list(data_lists[0])
test_df = build_df_from_list(data_lists[1])
val_df = build_df_from_list(data_lists[2])

train_df.label = train_df.label.map({0: "simple", 1: "hard", "simple": "simple",  "hard":  "hard"})
test_df.label = test_df.label.map({0: "simple", 1: "hard", "simple": "simple",  "hard":  "hard"})
val_df.label = val_df.label.map({0: "simple", 1: "hard", "simple": "simple",  "hard":  "hard"})

train_df.to_csv("../../data/bert_train.csv", index=False)
test_df.to_csv("../../data/bert_test.csv", index=False)
val_df.to_csv("../../data/bert_val.csv", index=False)
