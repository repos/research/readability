"""
Pre-process dumps of selected Wikipedias for batch-predictoin of readability scores.
"""
import time
import glob
import os
import re
import pickle
import numpy as np
import pandas as pd
import mwparserfromhell as mwph
import mwxml
from mwtokenizer.tokenizer import Tokenizer
from mwedittypes.utils import wikitext_to_plaintext

# Set arguments
FILE_WIKIS = "mbert_wikis.txt" # list of wikis to process
PATH_SAVE = "../intermediate/dumps/" # folder in which to save the pre-processed files
SNAPSHOT = "20230601"
N_CPU = 20 # how many cpus to run preprocessing in parallel


def map_wiki2lang(wiki_db):
    if wiki_db=="simplewiki":
        lang = "en"
    else:
        lang = wiki_db.replace("wiki","")
    return lang

def wikitext2text(wikitext):
    ## we select only the lead section
    first_section = re.search('={2,}.+?={2,}', wikitext)
    if first_section:
        wikitext_sec = wikitext[:first_section.span()[0]]
    else:
        wikitext_sec = wikitext
    
    # ## we replace <ref>-tags
    # filter_wikitext = re.sub(r"<\s*ref.*?\n?(<\s*/ref\s*>|/\s*>)", "", wikitext_sec)
    # wikicode = mwph.parse(filter_wikitext)
    # text = wikicode.strip_code()
    text = wikitext_to_plaintext(wikitext_sec)
    return text

def wikitext2sentences(wikicode, tokenizer):
    # needs proper parsing
    text = ""
    try:
        text = wikitext2text(wikicode)
    except:
        pass
    sents = []
    paragraphs = text.split("\n") # sentence tokenizer assumes paragraphs
    for par in paragraphs:
        if len(par.strip())>0:
            for sent in tokenizer.sentence_tokenize(par):
                # some additional heuristic filters
                if "|" in sent:
                    continue
                if len(sent)<10:
                    continue
                # add to list of sentences
                sents.append(sent)
    return sents

def process_dump(dump, path):
    data = []
    wiki_db = dump.site_info.dbname
    lang = map_wiki2lang(wiki_db)
    tokenizer = Tokenizer(language_code=lang)
    n_processed = 0
    for page in dump:
        if page.redirect or page.namespace != 0:
            continue
        wikicode = next(page).text
        page_title=page.title
        page_id = page.id
        page_sentences = wikitext2sentences(wikicode, tokenizer) 
        if len(page_sentences)>0:
            data_i = {
                 'sample_id': int(page_id),
                 'title': page_title,
                 'text': page_sentences,
                 # 'label': Optional[str]
                }
            data.append(data_i)
        n_processed += 1
    yield path, data, wiki_db

    
# Select wikis
list_wiki_db = []
with open(FILE_WIKIS,"r") as fin:
    for line in fin:
        wiki_db = line.strip()
        if wiki_db.endswith("wiki"):
            list_wiki_db += [wiki_db]
dict_wikidb_n = {wiki_db:0 for wiki_db in list_wiki_db}
print("Will parse %s wikis"%len(list_wiki_db))

# Select dump files
# get files from xml-dumps
paths_all = []
for wiki_db in list_wiki_db:
    paths = []
    dump_fn = "/mnt/data/xmldatadumps/public/{0}/{1}/{0}-{1}-pages-articles.xml.bz2".format(
        wiki_db, SNAPSHOT
    )
    for infile in glob.glob(
        "/mnt/data/xmldatadumps/public/{0}/{1}/{0}-{1}-pages-articles*.xml*.bz2".format(
            wiki_db, SNAPSHOT
        )
    ):
        if infile == dump_fn:
            continue
        if "multistream" in infile:
            continue
        paths += [infile]
    if len(paths) == 0:
        paths += [dump_fn]
    paths_all+=paths
print("Parsing %s dump files"%(len(paths_all)))

FILES_sents = []
n_file = 0
t1=time.time()
for path, data, wiki_db in mwxml.map(process_dump, paths_all, threads=N_CPU):
    n_file = dict_wikidb_n[wiki_db]
    FNAME_save = os.path.join(PATH_SAVE,"tmp_%s_%s_%s_sentences.pickle"%(wiki_db,SNAPSHOT,n_file))
    with open(FNAME_save, 'wb') as handle:
        pickle.dump(data, handle)
    FILES_sents+=[FNAME_save]
    dict_wikidb_n[wiki_db] += 1
t2=time.time()
print("Finished processing dumps in %.1f seconds"%(t2-t1))
