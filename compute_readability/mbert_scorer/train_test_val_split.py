import bz2, json
import numpy as np

TRAIN_SIZE = 0.8
TEST_SIZE = 0.1
VAL_SIZE = 0.1  # all remaining from train and test
RANDOM_SEED = 42
DATA_FILE_PATH = "../../data/simple-en-wiki_text.bz2"
SAVE_PATH_TEMPLATE = "../../data/simple-en-{}.bz2"

data = []
with bz2.open(DATA_FILE_PATH, "rt") as fin:
    for line in fin:
        dict_in = json.loads(line)
        data.append(dict_in)

print("Length of data: ", len(data))
idx_list = np.arange(len(data))

# select train idx
np.random.seed(RANDOM_SEED)
train_size = int(len(data) * TRAIN_SIZE)
train_idx = np.random.choice(idx_list, size=train_size, replace=False)

# select validation idx
np.random.seed(RANDOM_SEED)
val_size = int(len(data) * VAL_SIZE)
samples_remained = list(set(idx_list) - set(train_idx))
val_idx = np.random.choice(samples_remained, size=val_size, replace=False)

# select test idx
test_idx = list(set(samples_remained) - set(val_idx))

train_articles = [data[i] for i in train_idx]
test_articles = [data[i] for i in test_idx]
val_articles = [data[i] for i in val_idx]
print(
    f"Length of train: {len(train_articles)}, length of test: {len(test_articles)}, length of val: {len(val_articles)}")

parts = [train_articles, test_articles, val_articles]
parts_names = ["train", "test", "validation"]

for sample, sample_name in zip(parts, parts_names):
    with open(SAVE_PATH_TEMPLATE.format(sample_name), 'w') as fout:
        for dic in sample:
            data_ = json.dumps(dic)
            fout.write(data_)
            fout.write("\n")

print("Data processed and saved.")
