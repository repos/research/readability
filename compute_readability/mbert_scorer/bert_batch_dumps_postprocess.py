"""
Post-processing (formatting etc) of predicted readabilty scores from dump files.
"""
import os
import glob
import pandas as pd
import numpy as np
from tqdm import tqdm
import pickle

data_path = "../intermediate/dumps/"
snapshot = "20230601"
data_save = "../readability-scores_dumps-mbert_%s.tsv.gz"%(snapshot) # compress the tsv to save space

list_files = []
for f in glob.glob(os.path.join(data_path, "tmp_*wiki_%s_*_scores.pickle"%(snapshot))):
    list_files+=[f]
list_files = sorted(list_files)
df = pd.DataFrame()
for filename_read in list_files:
    wiki_db = filename_read.split("_%s"%snapshot)[0].split("tmp_")[1]
    with open(filename_read,"rb") as handle:
        data = pickle.load(handle)
    data_formatted = [
        {
            "wiki_db": wiki_db,
            "page_id": h["sample_id"],
            "page_title": h["title"],
            "score_level_predict": h["score"]["score"],
            "score_fk_predict": h["score"]["fk_score"]
        } for h in data]
    df_tmp = pd.DataFrame(data_formatted)
    df = pd.concat([df,df_tmp])
df.to_csv(data_save,sep="\t", index=False, compression="gzip")
