import json
import pickle

# preparing data for batch prediction
testing_dataset = '../../data/simple-en-test.bz2'

i = 0
testing_data = []
with open(testing_dataset, 'r') as fout:
    for line in fout:
        dict_in = json.loads(line)
        text_easy = dict_in["easy"]
        text_hard = dict_in["hard"]
        text_easy["label"] = "easy"
        text_hard["label"] = "hard"
        text_easy["sample_id"] = str(i)
        text_hard["sample_id"] = str(i+1)
        testing_data.append(text_easy)
        testing_data.append(text_hard)
        i += 2

with open('../../data/sample_for_batch_predict.pickle', 'wb') as handle:
    pickle.dump(testing_data, handle)
