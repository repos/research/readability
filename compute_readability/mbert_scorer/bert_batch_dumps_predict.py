"""
Bulk prediction of readability scores for pre-processed dump files.
"""
import time
import os
import glob
import pandas as pd
import numpy as np
from tqdm import tqdm
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, TrainingArguments, Trainer, pipeline
from catboost import CatBoostRegressor
import pickle
from bert_batch_predict import mean_pooling, max_pooling, min_pooling, predictions_processing, get_scores, batch_predict, load_models

data_path = "../intermediate/dumps/"
bert_model_path = "/home/trokhymovych/01_readability/models/mbert_readability"
fk_model_path = "/home/trokhymovych/01_readability/fk_scores.bin"
snapshot = "20230601"
batch_size = 128

# loading models:
bert_model, fk_scorer = load_models(bert_model_path, fk_model_path)

# iterating through all files
list_files = []
for f in glob.glob(os.path.join(data_path, "tmp_*wiki_%s_*_sentences.pickle"%(snapshot))):
    list_files+=[f]
list_files = sorted(list_files)
n_files = len(list_files)
i_files = 0
start_time_global = time.time()
for filename_read in list_files:
    i_files+=1
    print("processing file %s out of %s"%(i_files,n_files))
    with open(filename_read, 'rb') as handle:
        testing_dataset = pickle.load(handle)
    # batch prediction
    start_time = time.time()
    predictions_gpu = batch_predict(bert_model, fk_scorer, testing_dataset)
    time_spent = round(time.time() - start_time, 2)

    print(f"Total time spend: {time_spent} s")
    print(f"Time per sample: {round(time_spent / len(testing_dataset), 3)} s")

    # saving results
    for sample in testing_dataset:
        sample["score"] = predictions_gpu[sample["sample_id"]]
        del sample["text"]
    
    filename_save = filename_read.replace("_sentences.pickle","_scores.pickle")
    with open(filename_save, 'wb') as handle:
        pickle.dump(testing_dataset, handle)
print("Finished processing %s files"%i_files)
time_spent = round(time.time() - start_time_global, 2)
print(f"Total time spend: {time_spent} s")