import time
import pandas as pd
import numpy as np
from tqdm import tqdm
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, TrainingArguments, Trainer, pipeline
from catboost import CatBoostRegressor
from argparse import ArgumentParser
import pickle

parser = ArgumentParser()
parser.add_argument('--data_path', type=str, required=True, help='path to data file')
parser.add_argument('--bert_model_path', type=str, required=True, help='path to bert model')
parser.add_argument('--fk_model_path', type=str, required=True, help='path to fk-scoring model')
parser.add_argument('--batch_size', type=int, required=False, default=128, help='batch size')

args = parser.parse_args()
data_path = args.data_path
bert_model_path = args.bert_model_path
fk_model_path = args.fk_model_path
batch_size = args.batch_size

def mean_pooling(sentences_scores) -> float:
    """
    Method that aggregate the scores for the sentences using mean
    """
    if len(sentences_scores) > 0:
        return float(np.mean(sentences_scores))
    else:
        return 0


def max_pooling(sentences_scores) -> float:
    """
    Method that aggregate the scores for the sentences using max
    """
    if len(sentences_scores) > 0:
        return float(np.max(sentences_scores))
    else:
        return 0


def min_pooling(sentences_scores) -> float:
    """
    Method that aggregate the scores for the sentences using min
    """
    if len(sentences_scores) > 0:
        return float(np.min(sentences_scores))
    else:
        return 0


def predictions_processing(predictions):
    res = []
    for i in predictions:
        res.append(i[1]['score'])
    return res


def get_scores(sample_results_df, fk_model):
    # filling 'bert_score_mean', 'bert_score_max', 'bert_score_min'
    features = {
        'bert_score_mean': mean_pooling(sample_results_df["score"]),
        'bert_score_max': max_pooling(sample_results_df["score"]),
        'bert_score_min': min_pooling(sample_results_df["score"]),
    }
    # filling 'sent_N'
    for sent_id, score in zip(sample_results_df.sample_rank_id.values, sample_results_df.score.values):
        features["sent_" + str(sent_id)] = score
    # filling empty values
    for sent_id in range(1, 16):
        features["sent_" + str(sent_id)] = features.get("sent_" + str(sent_id), -1)
    fk_score = fk_model.predict([features[f] for f in fk_model.feature_names_])
    result = pd.DataFrame(
        {
            "sample_id": [sample_results_df.sample_id.values[0]],
            "fk_score": [fk_score],
            "score": [features["bert_score_mean"]]
        }
    )
    return result


def batch_predict(bert_pipeline, fk_model, testing_data):
    ### Preparing texts for prediction
    sentences = []
    sample_ids = []
    sample_rank_id = []
    for sample in testing_data:
        sentences += sample["text"]
        sample_ids += [sample["sample_id"]] * len(sample["text"])
        sample_rank_id += list(np.arange(1, len(sample["text"])+1))

    ### Sorting sentences by the length to make the batch processing more efficient:
    texts_length = [len(t) for t in sentences]
    ids = np.argsort(texts_length)[::-1]
    sentences_sorted = [sentences[i] for i in ids]
    sample_ids_sorted = [sample_ids[i] for i in ids]
    sample_rank_id_sorted = [sample_rank_id[i] for i in ids]

    ### Predicting using pipeline:
    tqdm_batch_size = batch_size * 10
    predicted_scores = []
    for i in tqdm(range(0, len(sentences_sorted), tqdm_batch_size)):
        # sorting texts for better performance:
        texts_batch = sentences_sorted[i:i + tqdm_batch_size]
        tokenizer_kwargs = {'truncation': True, 'max_length': 512}
        predicted_scores += bert_pipeline(
            texts_batch,
            return_all_scores=True,
            **tokenizer_kwargs,
            batch_size=batch_size
        )
    ### Reassign function:
    get_fk_score_local = lambda x: get_scores(x, fk_model)
    ### Aggregating scores:
    tmp_df = pd.DataFrame({
        "sample_id": sample_ids_sorted,
        "sample_rank_id": sample_rank_id_sorted,
        "score": predictions_processing(predicted_scores)
    })
    tmp_df_scores = tmp_df.groupby('sample_id').apply(get_fk_score_local).reset_index(drop=True)
    tmp_df_scores.index = tmp_df_scores["sample_id"]
    return tmp_df_scores.to_dict("index")


def load_models(bert_model: str, fk_model: str):
    # loading the model
    device = 0
    tokenizer = AutoTokenizer.from_pretrained(bert_model, truncation=True, max_length=512, device=device)
    model = AutoModelForSequenceClassification.from_pretrained(bert_model).to(device)
    clf = pipeline(task="text-classification", model=model, tokenizer=tokenizer, device=device, batch_size=batch_size)
    fk_scorer = CatBoostRegressor()
    fk_scorer.load_model(fk_model)
    return clf, fk_scorer


# loading models:
bert_model, fk_scorer = load_models(bert_model_path, fk_model_path)

# loading the data:
with open(data_path, 'rb') as handle:
    testing_dataset = pickle.load(handle)

# batch prediction
start_time = time.time()
predictions_gpu = batch_predict(bert_model, fk_scorer, testing_dataset)
time_spent = round(time.time() - start_time, 2)

print(f"Total time spend: {time_spent} s")
print(f"Time per sample: {round(time_spent / len(testing_dataset), 3)} s")

# saving results
for sample in testing_dataset:
    sample["score"] = predictions_gpu[sample["sample_id"]]
with open('../../data/sample_for_batch_predict_processed.pickle', 'wb') as handle:
    pickle.dump(testing_dataset, handle)
