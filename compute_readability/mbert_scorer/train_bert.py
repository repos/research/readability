import numpy as np
from datasets import load_dataset, load_metric, Dataset, ClassLabel, set_caching_enabled
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, TrainingArguments, Trainer


training_dataset = Dataset.from_csv("../../data/bert_train.csv")
feat_class = ClassLabel(num_classes=2, names=["simple", "hard"])
training_dataset = training_dataset.cast_column("label", feat_class)

# We use a small local hold-out test set (1%) for training sentence-based classifier.
# This evaluation is different because we don`t consider articles/documents at this stage but only sentences.
# It is important to use a small portion to evaluate that part, so we don`t overfit
training_dataset = training_dataset.train_test_split(test_size=0.01, stratify_by_column="label", shuffle=True, seed=42)

# tokenization:
model_checkpoint = "bert-base-multilingual-cased"
tokenizer = AutoTokenizer.from_pretrained(model_checkpoint)

sentence1_key = "text"
sentence2_key = None

def preprocess_function(examples):
    if sentence2_key is None:
        return tokenizer(examples[sentence1_key], truncation=True, max_length=512)
    return tokenizer(examples[sentence1_key], examples[sentence2_key], truncation=True, max_length=512)

encoded_dataset = training_dataset.map(preprocess_function, batched=True)


num_labels = 2
metric_name = "accuracy"
batch_size = 10
model = AutoModelForSequenceClassification.from_pretrained(model_checkpoint, num_labels=num_labels)

args = TrainingArguments(
    f"models/bert_tuned",
    evaluation_strategy = "epoch",
    save_strategy = "epoch",
    learning_rate=2e-5,
    per_device_train_batch_size=batch_size,
    per_device_eval_batch_size=batch_size,
    num_train_epochs=5,
    weight_decay=0.01,
    load_best_model_at_end=True,
    metric_for_best_model=metric_name,
    push_to_hub=False,
)

metric = load_metric("glue", "mrpc")
def compute_metrics(eval_pred):
    predictions, labels = eval_pred
    predictions = np.argmax(predictions, axis=1)
    return metric.compute(predictions=predictions, references=labels)

trainer = Trainer(
    model,
    args,
    train_dataset=encoded_dataset["train"],
    eval_dataset=encoded_dataset["test"],
    tokenizer=tokenizer,
    compute_metrics=compute_metrics
)

trainer.train()
trainer.evaluate()
