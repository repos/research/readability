# Masked Language Model (BERT) based model logic

This directory contains the logic needed to fine-tune and use the bert model on inference.

1. It contains the logic for train-test-val splitting. 
`train_test_val_split.py` includes the logic for splitting the English dataset for the experiment.
```bash
python train_test_val_split.py
```

2. It contains the logic for preparing the training file (dataframe) needed for further bert model tuning 
`bert_data_prepare.py` includes the logic for preparing bert training input. 
As we train sentence-based models, specific data preparation is needed. 
```bash
python bert_data_prepare.py
```

3. It contains the script for bert model tuning. 
It uses the file prepared during previous steps, tunes the model, and saves checkpoints for each epoch. 
By default, the script will be using GPU in case the environment is set up correctly. 
In order to run it using GPU, the specific version of pytorch should be installed. 
(It depends on the version of CUDA/ROCm). 
The following setup was working fine on stat1008/5: `pip install torch==1.10.1+rocm4.2 torchvision==0.11.2+rocm4.2 torchaudio==0.10.1 -f https://download.pytorch.org/whl/rocm4.2/torch_stable.html`

```bash
python train_bert.py
```

4. It contains the script that can be used for model inference `mbert_scorer_base.py`
The example of inference usage is presented in `compute_readability/mbert_readibility_inference.ipynb`

5. It also contains scripts for efficient batch prediction using GPU.

`bert_batch_predict.py` script contains the function `batch_predict` that takes pipeline and data as input and returns scores for each data sample.
Data should be provided in the form of list of dictionaries with the following structure:
```json
{
     'sample_id': str,
     'title': Optional[str],
     'text': list[str],
     'label': Optional[str]
}
```
Where `sample_id` is a unique identifier of the sample, `text` is the list of sentences.
There is an example script for this specific data format preparation: `bert_batch_data_preprocessing.py`

In this script, it is hardcoded to use GPU (GPU env setup is presented earlier). 
The reason is that script was specifically created for the purpose of fast batch prediction.
It was tested different setups and optimized the code, so it can reach the efficiency of ±0.015s per article (on stat1005/8 GPU). (only bert inference is counted)

Example script of batch prediction usage:
```bash
python bert_batch_predict.py --data_path=../../data/sample_for_batch_predict.pickle --bert_model_path=/home/trokhymovych/01_readability/models/mbert_readability  --fk_model_path=/home/trokhymovych/01_readability/fk_scores.bin
```
