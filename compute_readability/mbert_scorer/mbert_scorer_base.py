import numpy as np
from typing import Dict, List, Union
from transformers import AutoTokenizer, AutoModelForSequenceClassification, pipeline

from .utils import get_article_text, wikitext2text, text2sentences

class MBERTReadabilityScorerBase:
    """
    Class that implements the logic of readability scoring using fine-tuned MLM (mbert)
    Base implementation is using simple mean pooling of scores for each independent sentence score
    """

    def __init__(
        self,
        model_checkpoint: str,
        device: Union[str, int] = 0,
        batch_size: int = 128,
        max_length: int = 512,
        truncation: bool = True
    ) -> None:
        tokenizer = AutoTokenizer.from_pretrained(model_checkpoint, truncation=truncation, max_length=max_length,
                                                  device=device)
        model = AutoModelForSequenceClassification.from_pretrained(model_checkpoint).to(device)
        self.clf = pipeline(task="text-classification", model=model, tokenizer=tokenizer,
                            batch_size=batch_size, device=device)
        self.max_length = max_length
        self.truncation = truncation
        self.batch_size = batch_size

    def predict_sentences_scores(self, sentences: List[str]) -> List[float]:
        tokenizer_kwargs = {'truncation': self.truncation, 'max_length': self.max_length}
        raw_predictions = self.clf(sentences, return_all_scores=True, **tokenizer_kwargs, batch_size=self.batch_size)
        scores = self._process_mlm_predictions(raw_predictions)
        return scores

    def get_wiki_page_score(self, page_name: str, api_url: str = "https://en.wikipedia.org/w/api.php") -> float:
        """
        Method that implements scoring of the given page
        It gets the text of the given page, parse it, splits to sentences and make a prediction
        # ToDo: probably should be changed to revision instead.
        """
        wikitext = get_article_text(page_name, api_url=api_url)
        text = wikitext2text(wikitext)
        sentences = text2sentences(text)
        sentences_scores = self.predict_sentences_scores(sentences)
        return self._scores_pooling(sentences_scores)

    @staticmethod
    def _scores_pooling(sentences_scores: List[float], **kwargs) -> float:
        """
        Method that aggregate the scores for the sentences and return the page scoring.
        In basic implementation is a simple mean pooling.
        It can be reimplemented with more sophisticated approach,
            in such a case additional parameters then should be passed in **kwargs.
        """
        return np.mean(sentences_scores)

    @staticmethod
    def _process_mlm_predictions(predictions: List[Dict]) -> List[float]:
        res = []
        for i in predictions:
            res.append(i[1]['score'])
        return res
