## Measuring Readability of Wikipedia Articles

This folder contains the code for generating and applying various readability metrics for the articles in the data folder.

### Subfolders:
- error_logs/: contains errors from matching simple-wiki articles (when the titles don't match)
- models/: contains the supervised models (with and without gridsearch)
- intermediate/: contains pickled dictionaries with various readability metrics for simple-wiki articles and wiki-viki articles including the FRE readability scores and the entities. [TODO: add the wikidata entry]

### Notebooks:
- **data explore.ipynb**: get an overview of the simple-en and wiki-viki data, load the klexikon data and standardize it like the wiki-viki data and only retain the lede section.
- **score english data with readability formulae.ipynb**: get the Simple and English language Wikipedia editions and score them with 6 formula-based readability metrics, see the distribution of the scores and their correlation with each other.
- **score entity based metrics.ipynb**: get the Simple and English language Wikipedia editions, annotate entities in them with db-spotlight, get the language agnostic features and save them in a pickled dictionary.
- **multilingual analysis (readability and entities).ipynb**: get the multilingual Wikipedia-Vikidia data, get the redability scores (customized and non-customized), annotate entities in them with db-spotlight, get the language agnostic features and save them in a pickled dictionary.
- **supervised learning training.ipynb**: extract all features for all Simple-Wiki ~85K pairs (170K articles) and train classifiers using them where the goal is predicting whether the document is simple or not. Split the data into train-test splits, while keeping the pairs intact, i.e., a simple wikipedia articles's counterpart should be in the same split. Find hyperparameters using gridsearcg and save the best models, where best is evaluated using k-fold cross validation.
- **supervised learning evaluation.ipynb**: evaluate the supervised readability models on various test data 

### Other files:
- get_entities.py
- utils.py
- custom_textstat.py
