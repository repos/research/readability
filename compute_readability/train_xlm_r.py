import os
os.environ["WANDB_DISABLED"] = "true"

from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, Trainer, TrainingArguments
import torch

import numpy as np
from sklearn.metrics import classification_report

import pickle
from random import shuffle

from datasets import Dataset

import numpy as np
from sklearn.metrics import accuracy_score, mean_absolute_error

def compute_metrics(eval_pred):
    predictions, labels = eval_pred
    predictions = np.argmax(predictions, axis=1)
    return {"accuracy": accuracy_score(labels, predictions)}

def tokenize_articles(examples):
    return tokenizer(examples["text"], truncation=True, max_length=512)

model_checkpoint = "xlm-roberta-base"
tokenizer = AutoTokenizer.from_pretrained(model_checkpoint)

LR = 2e-5
EPOCHS = 1
BATCH_SIZE = 32
MAX_TRAINING_EXAMPLES = -1 # set this to -1 if you want to use the whole training set

runs = 5


with open('intermediate/english_train_text_splits', 'rb') as handle:
    paired_trains = pickle.load(handle) 
    
with open('intermediate/english_test_text_splits', 'rb') as handle:
    paired_tests = pickle.load(handle) 


# map the labels to numerical ones
for i in range(runs):
    paired_trains[i]['label'] = [1 if row['edition'] == 'en' else 0 for _, row in paired_trains[i].iterrows()]
    paired_tests[i]['label'] = [1 if row['edition'] == 'en' else 0 for _, row in paired_tests[i].iterrows()]


for run in runs:
    paired_train = paired_trains[run].iloc[train_index_doubled]
    paired_val = paired_trains[run].iloc[val_index_doubled]

    train = Dataset.from_pandas(paired_train)
    val = Dataset.from_pandas(paired_val)

    tokenized_dataset = train.map(tokenize_articles, batched=True)
    tokenized_dataset_val = val.map(tokenize_articles, batched=True)


    num_labels = 2
    model = AutoModelForSequenceClassification.from_pretrained(model_checkpoint,
        num_labels=num_labels)


    model_name = model_checkpoint.split("/")[-1]
    batch_size = 16
    num_train_epochs = 2
    logging_steps = len(tokenized_dataset) // (batch_size * num_train_epochs)

    args = TrainingArguments(
        output_dir="models/xlm-roberta-base_%d/" %run,
        evaluation_strategy = "epoch",
        save_strategy = "epoch",
        learning_rate=2e-5,
        per_device_train_batch_size=batch_size,
        per_device_eval_batch_size=batch_size,
        num_train_epochs=num_train_epochs,
        weight_decay=0.01,
        logging_steps=logging_steps
    )

    trainer = Trainer(
        model,
        args,
        train_dataset=tokenized_dataset,
        eval_dataset=tokenized_dataset_val,
        tokenizer=tokenizer,
        compute_metrics=compute_metrics
    )

    trainer.train()
