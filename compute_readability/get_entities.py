import json
import pandas as pd
import ast
import pickle 
import spacy

def apply_dbpedia(datapoint_wiki):
    nlp = spacy.blank('en')
    nlp.add_pipe('dbpedia_spotlight', config={'confidence': 0, 'overwrite_ents':False})
    docs = datapoint_wiki['text']
    entities = []
    for text in docs:
        try:
            doc = nlp(text)
            entities.append([(ent.text, ent.kb_id_,
             ent._.dbpedia_raw_result['@similarityScore']) for ent in doc.ents])
        except Exception as e:
            print(e)
            print('!!!!!!!!!!!!!!!!!!!!!!!')
            # print(text)
            pass
        
    return entities


datapath = '../data/'

simple_en = pd.read_csv(datapath + 'simple_wikipedia/simple-en-wiki_common-articles-filtered.csv')

with open(datapath + "simple_wikipedia/simple-en-wiki_text", 'r') as file:
    lines = file.readlines()

data = []
for line in lines:
    data.append(ast.literal_eval(line))


for n, datapoint in enumerate(data):
    if n % 100 == 0:
        print(n, ' done...')
        with open('intermediate/simple_en_entities.pickle', 'wb') as handle:
            pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)        

    for wiki in ['en', 'simple']:
        datapoint[wiki]['entities'] = apply_dbpedia(datapoint[wiki])    
    


with open('intermediate/simple_en_entities.pickle', 'wb') as handle:
    pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)
