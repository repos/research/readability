import re
import mwparserfromhell
import nltk
import requests
import json
import numpy as np


dict_abbreviations = {
    # https://en.wikipedia.org/wiki/List_of_German_abbreviations
    "de": [
        "am","amtl","Anh","Ank","Anl","Anm","anschl","a.o","App","a.Rh","Art","Aufl","Ausg",\
        "b","Bd","Bde","beil","bes","bspw","Best.-Nr","Betr","Bev","Bez","Bhf","brit","b.w","bzgl","bzw",\
        "ca","Chr",\
        "d.Ä","dazw","desgl","dgl","d.h","d.i","Dipl","Dipl.-Ing","Dipl.-Kfm","Dir","d.J","d.M","d.O","d.o","Dr","dt","Dtzd"\
        "e.h","ehem","eigtl","einschl","engl","entspr","erb","Erw","erw","ev","e.V","evtl","e.Wz","exkl",\
        "f","ff","Fa","Fam","Ffm","Fr","fr","Frl","Frfr","frz",\
        "geb","Gebr","gedr","gegr","gek","Ges","gesch","geschl","geschr","ges.gesch","gest","gez","ggf","ggfs",\
        "Hbf","hpts","Hptst","Hr","Hrn","Hrsg",\
        "i.A","i.b","i.b","i.B","i.D","i.H","i.J","Ing","Inh","inkl","i.R","i.V","inzw",\
        "jew","Jh","jhrl",\
        "k.A","Kap","kath","Kfm","kfm","kgl","Kl","kompl",\
        "l","led","L.m.a.A",\
        "m.a.W","max","med","m.E","Mil","Mio","m.M","m.M.n","möbl","Mrd","Msp","mtl","mdl","m.ü.M","m.W","MwSt","Mw-St",\
        "n","näml","n.Chr","n.J","nördl","norw","Nr","n.u.Z",\
        "o","o.A","o.a","o.ä","o.B","od","o.g","österr",\
        "p.Adr","Pfd","Pkt","Pl","phil",\
        "r","Reg.-Bez","r.k","r.-k","röm.-kath","röm",\
        "S","s","s.a","Sa","schles","schwäb","schweiz","sek","s.o","sog","St","Std","Str","s.u","südd",\
        "tägl","Tel",\
        "u","u.a","u.ä","u.Ä","u.a.m","u.A.w.g","übl","üblw","usw","u.v.a","u.v.a.m","u.U","u.zw",\
        "v","V","v.a","v.Chr","Verf","verh","verw","vgl","v.H","vorm","v.R.w","v.T","v.u.Z",\
        "wstl","w.o",\
        "z","Z","z.B","z.Hd","Zi","z.T","Ztr","zur","zus","zzgl","z.Z","z.Zt"
    ]
}

# mapping between wikipedia-languages and nltk-languages
dict_lang_nltk = {
    "cs":"czech",
    "da":"danish",
    "de":"german",
    "el":"greek",
    "en":"english",
    "es":"spanish",
    "et":"estonian",
    "fi":"finnish",
    "fr":"french",
    "it":"italian",
    "nl":"dutch",
    "nn":"norwegian",
    "no":"norwegian",
    "simple":"english",
    "pl":"polish",
    "pt":"portuguese",
    "ru":"russian",
    "sl":"slovene",
    "sv":"swedish",
    "tr":"turkish"
}


def wikitext2text(wikitext):
    ## we select only the lead section
    first_section = re.search('={2,}.+?={2,}', wikitext)
    if first_section:
        wikitext_sec = wikitext[:first_section.span()[0]]
    else:
        wikitext_sec = wikitext

    ## we replace <ref>-tags
    filter_wikitext = re.sub(r"<\s*ref.*?\n?(<\s*/ref\s*>|/\s*>)", "", wikitext_sec)
    wikicode = mwparserfromhell.parse(filter_wikitext)

    text = wikicode.strip_code()
    return text

def text2sentences(text, lang="en"):
    # map to nltk's language (default: english)
    lang_nltk = dict_lang_nltk.get(lang,"english")
    
    # full stop bengali
    text = text.replace("।", ".\n") 
    # full stop armenian (this is not the same as colon)
    text = text.replace("։",".\n")
    
    sentences = []
    # loading the sentence-tokenizer
    try:
        sentence_tokenizer = nltk.data.load(f"tokenizers/punkt/{lang_nltk}.pickle")
    except:
        sentence_tokenizer = nltk.data.load(f"tokenizers/punkt/english.pickle")
    
    # adding additional abbrevations: https://stackoverflow.com/questions/14095971/how-to-tweak-the-nltk-sentence-tokenizer/25375857#25375857
    additional_abbreviations = dict_abbreviations.get(lang,[])
    sentence_tokenizer._params.abbrev_types.update(additional_abbreviations)

    for line in text.split("\n"):
        for sent in sentence_tokenizer.tokenize(line):
            # formatting with whitespaces
            if len(sent)<2:
                continue
            # remove trailing navigation links to categories etc
            if sent[-1]!=".":
                continue
            # remove captions from images (not handled well by mwparserfromhell)
            # https://github.com/earwig/mwparserfromhell/issues/169
            if "|" in sent:
                continue
            sentences+=[sent]
    return sentences

def get_article_text(page_title, api_url):
#     api_url = "https://en.vikidia.org/w/api.php"
#     api_url = "https://en.wikipedia.org/w/api.php"
    headers = {"User-Agent": "readability: MGerlach_(WMF)"}    
    params = {
        "action": "query",
        "prop": "revisions",
        "rvprop": "content",
#         "rvsection": "0",
        "titles": page_title,
        "format": "json",
        "formatversion": "2",
}
    req = requests.get(api_url, headers=headers, params=params).json()
    req_query = req.get("query")
    req_pages = req_query.get("pages")
    page = req_pages[0]
    wikitext = page.get("revisions",[])[0].get("content","")

    return wikitext