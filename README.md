# readability

## Generating datasets

### Simple-English-Wikipedia

Dataset of articles that are common to Simple and English Wikipedia. Matching of articles is done via their Wikidata-items. The following filters are applied:
- only articles from the main namespace and non-redirects
- remove disambiguation and list pages
- only keep text from lead section
- only keep articles if both version's text contains at least 3 sentences

The final dataset contains 85,626 articles in English each in two versions: `data/simple-en-wiki_text.bz2`

Execute the following notebooks:
- `create-datasets_simple-en-wiki_get-common-articles.ipynb`
- `create-datasets_simple-en-wiki_get-article-content.ipynb`
- `create-datasets_simple-en-wiki_filter-wikitext.ipynb`


### Vikidia-Wikipedia
Dataset of articles that are common to [Vikidia](https://www.vikidia.org/) (a children encyclopedia) and [Wikipedia](https://www.wikipedia.org/) in 12 different languages: French (fr), Italian (it), Spanish (es), English (en), Basque (eu), Armenian (hy), German (de), Catalan (ca), Sicilian (scn), Russian (ru), Portuguese (pt), Greek (el). Matching of articles is done via their titles (and all of their redirects). The following filters are applied:
- only articles from the main namespace and non-redirects
- only articles which can be matched unambiguously
- only keep text from lead section
- only keep articles if both version's text contains at least 3 sentences

The final datasets contain articles for each language each in two versions: `data/vikiwiki-<lang>_text.bz2` (fr: 9893, it: 1157, es: 2136, en: 1729, eu: 945, hy: 6, de: 256, ca: 211, scn: 8, ru: 106, pt: 40, el: 33)


Execute the following notebooks:
- `create-datasets_vikiwiki_get-vikidia-articles.ipynb`
- `create-datasets_vikiwiki_get-common-articles.ipynb`
- `create-datasets_vikiwiki_get-article-content.ipynb`
- `create-datasets_vikiwiki_filter-wikitext.ipynb`

Note that there are other wikis for children:
- overview [Meta: List of wikis for children](https://meta.wikimedia.org/wiki/List_of_wikis_for_children)
- nl: https://wikikids.nl/
- de: https://klexikon.zum.de/
- ...
